package ru.tsystem.javabaseconfig.dao;

import ru.tsystem.javabaseconfig.domain.Person;

public class PersonDaoSimple implements PersonDao {

    public Person getPerson() {
        return new Person("Ivan", 18);
    }
}
