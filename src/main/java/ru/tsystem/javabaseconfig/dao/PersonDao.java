package ru.tsystem.javabaseconfig.dao;

import ru.tsystem.javabaseconfig.domain.Person;

public interface PersonDao {

    Person getPerson();
}
