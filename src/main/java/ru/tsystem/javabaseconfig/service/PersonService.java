package ru.tsystem.javabaseconfig.service;

import ru.tsystem.javabaseconfig.domain.Person;

public interface PersonService {

    Person getPerson();
}
