package ru.tsystem.javabaseconfig.service;

import ru.tsystem.javabaseconfig.dao.PersonDao;
import ru.tsystem.javabaseconfig.domain.Person;

public class PersonServiceImpl implements PersonService {

    private final PersonDao dao;

    public PersonServiceImpl(PersonDao dao) {
        this.dao = dao;
    }

    public Person getPerson() {
        return dao.getPerson();
    }
}
