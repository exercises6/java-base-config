package ru.tsystem.javabaseconfig.config;

import ru.tsystem.javabaseconfig.dao.PersonDao;
import ru.tsystem.javabaseconfig.dao.PersonDaoSimple;


public class DaoConfig {

    public PersonDao personDao() {
        return new PersonDaoSimple();
    }
}
