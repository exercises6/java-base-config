package ru.tsystem.javabaseconfig.config;


import ru.tsystem.javabaseconfig.dao.PersonDao;
import ru.tsystem.javabaseconfig.service.PersonService;
import ru.tsystem.javabaseconfig.service.PersonServiceImpl;


public class ServicesConfig {

    public PersonService personService(PersonDao dao) {
        return new PersonServiceImpl(dao);
    }
}
